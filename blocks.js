const Blocks = function (init) {
    let numOfCells = init.rows * init.cols;
    let x = 0;
    let y = 0;
    let coordinates = [];
    let divs = [];

    function _addStyle(object, styles) {
        for (let style in styles) {
            if (styles.hasOwnProperty(style)) {
                object.style[style] = styles[style];
            }
        }
    }

    function _randNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function _randColor() {
        let red = _randNumber(0, 255);
        let green = _randNumber(0, 255);
        let blue = _randNumber(0, 255);

        return "rgb(" + red + ", " + green + ", " + blue + ")";
    }

    function shuffle() {
        let newCoordinates = [],
            coordinatesLen = coordinates.length;

        for (let i = 0; i < coordinatesLen; i++) {
            let currCoordinatesLen = coordinates.length;
            let rand = _randNumber(0, currCoordinatesLen - 1);
            let coordinate = coordinates[rand];

            newCoordinates.push(coordinate);
            coordinates = coordinates.slice(0, rand).concat(coordinates.slice(rand + 1, currCoordinatesLen));

            _addStyle(divs[i], {
                top: coordinate[0] + "px",
                left: coordinate[1] + "px"
            });
        }

        coordinates = newCoordinates;
    }

    /* Initialize */
    _addStyle(init.container, {
        width: init.cols * init.width + "px",
        height: init.rows * init.height + "px"
    });

    while (numOfCells-- > 0) {
        let div = document.createElement("div");

        _addStyle(div, {
            backgroundColor: _randColor(),
            top: y + "px",
            left: x + "px",
            width: init.width + "px",
            height: init.height + "px"
        });

        init.container.appendChild(div);
        coordinates.push([y, x]);
        divs.push(div);

        if (numOfCells % init.cols === 0) {
            y += init.height;
            x = 0;
        } else {
            x += init.width;
        }
    }

    return {
        shuffle: shuffle
    }
};